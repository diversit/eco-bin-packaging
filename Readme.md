# Java and Scala solution for Ecological Bin Problem.

Version 1.0

Both solutions solve the problem recursively. Therefore this solution supports any number of
bins and glasses as long as the total moves required is less than 2^64-1 and the glass count in a bin
is less than 2^31-1.
