package eu.diversit.ing.scala

import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ScalaEcoBinPackagingTest extends FlatSpec with ShouldMatchers {

  behavior of "Eco Bin Packaging solution"

  import ScalaEcoBinPackaging._
  "Solve (1 2 3 4 5 6 7 8 9)" should "return solution 'BCG 30'" in {
    solve("1 2 3 4 5 6 7 8 9") should equal("BCG 30")
  }

  "Solve (5 10 5 20 10 5 10 20 10)" should "return solution 'CBG 50'" in {
    solve("5 10 5 20 10 5 10 20 10") should equal("CBG 50")
  }

  "Solve (1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16)" should "return solution 'BCGR 102'" in {
    solve("1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16") should equal("BCGR 102")
  }

  "Solve (1 2 3 4 5 6)" should "fail because number of glasses and bins is not the same" in {
    intercept[AssertionError] {
      solve("1 2 3 4 5 6")
    }
  }

  "Solve (238609294 238609294 238609294 238609294 238609294 238609294 238609294 238609294 238609294" should "return solution BCG 1431655764" in {
    solve("238609294 238609294 238609294 238609294 238609294 238609294 238609294 238609294 238609294") should equal("BCG 1431655764")
  }

  "Solve (85263245 25965748 69854785 15874569 36985745 12365478 36985526 32147859 96587458" should "return solution BGC 193193965" in {
    solve("85263245 25965748 69854785 15874569 36985745 12365478 36985526 32147859 96587458") should equal("BGC 193193965")
  }

  "Solve (123 654 789 963 258 741 159 963 357" should "return solution CBG 2292" in {
    solve("123 654 789 963 258 741 159 963 357") should equal("CBG 2292")
  }

  "Solve (123 987 12 852 963 987 963 159 753)" should "return GCB 2862" in {
    solve("123 987 12 852 963 987 963 159 753") should equal("GCB 2862")
  }

  behavior of "SortedBins"

  "the name" should "be in order of bins added" in {
    val bins = SortedBins.empty
      .addBinWith(GlassAndMoves(Glass.Green, 10))
      .addBinWith(GlassAndMoves(Glass.Clear, 5))
      .addBinWith(GlassAndMoves(Glass.Brown, 25))

    bins.name should be ("GCB")
  }

  "the total" should "be the sum of all moves" in {
    val bins = SortedBins(GlassAndMoves(Glass.Green, 10))
      .addBinWith(GlassAndMoves(Glass.Clear, 5))
      .addBinWith(GlassAndMoves(Glass.Brown, 25))

    bins.totalMoves should be (40)
  }
}
