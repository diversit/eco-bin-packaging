package eu.diversit.ing.java;

import org.junit.Test;

import static eu.diversit.ing.java.JavaEcoBinPackaging.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class JavaEcoBinPackagingTest {

    @Test
    public void solve123456789ShouldReturnSolutionBCG30() {
        assertThat(solve("1 2 3 4 5 6 7 8 9"), is("BCG 30"));
    }

    @Test
    public void solve510520105102010ShouldReturnSolutionCGB50() {
        assertThat(solve("5 10 5 20 10 5 10 20 10"), is("CBG 50"));
    }

    @Test
    public void solve12345678910111213141516ShouldReturnSolutionBCGR102() {
        assertThat(solve("1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16"), is("BCGR 102"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void solve123456ShouldFailBecauseNumberOfGlassesAndBinsIsNotEqual() {
        solve("1 2 3 4 5 6");
    }

    @Test
    public void additionalTestsWithBigNumbers() {
        assertThat(solve("238609294 238609294 238609294 238609294 238609294 238609294 238609294 238609294 238609294"), is("BCG 1431655764"));
        assertThat(solve("85263245 25965748 69854785 15874569 36985745 12365478 36985526 32147859 96587458"), is("BGC 193193965"));
        assertThat(solve("123 654 789 963 258 741 159 963 357"), is("CBG 2292"));
        assertThat(solve("123 987 12 852 963 987 963 159 753"), is("GCB 2862"));
    }

    @Test
    public void sortedBinsNameShouldBeInOrderOfBinsAdded() {
        SortedBins bins = SortedBins.empty()
                .addBinWith(new GlassAndMoves(Glass.Green, 10))
                .addBinWith(new GlassAndMoves(Glass.Clear, 5))
                .addBinWith(new GlassAndMoves(Glass.Brown, 25));

        assertThat(bins.name(), is("GCB"));
    }

    @Test
    public void sortedBinsTotalShouldBeSumOfAllMoves() {
        SortedBins bins = SortedBins.empty()
                .addBinWith(new GlassAndMoves(Glass.Green, 10))
                .addBinWith(new GlassAndMoves(Glass.Clear, 5))
                .addBinWith(new GlassAndMoves(Glass.Brown, 25));

        assertThat(bins.totalMoves(), is(40L));
    }
}
