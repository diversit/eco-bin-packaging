package eu.diversit.ing.scala

import org.slf4j.LoggerFactory

trait Logging {
  val LOG = LoggerFactory.getLogger(this.getClass)
}

object ScalaEcoBinPackaging extends Logging {

  /**
   * @param problem 9 space seperated values defining the values for Brown, Green and Clear glass per bin
   * @return Minimum amount of moves needed to have single color glass in each bin
   */
  def solve(problem: String): String = {

    def toGlassAndMoves(bin: Array[Int]) = for {
        i <- 0 until bin.length
    } yield GlassAndMoves(Glass(i),bin.sum - bin(i))

    def calcSolutions(acc:List[SortedBins], sortedBins:SortedBins, unsortedBins: List[Seq[GlassAndMoves]]): List[SortedBins] = {
      if (sortedBins.size == unsortedBins.size) {
        sortedBins :: acc
      } else {
        val currentBin = unsortedBins(sortedBins.size)
        val bins = for {
          glass <- currentBin if (sortedBins notContainsBinWith glass)
        } yield {
          calcSolutions(acc, sortedBins.addBinWith(glass), unsortedBins)
        }
        bins.flatten.toList
      }
    }

    // split problem into list of Int's
    val allGlassesInBins = problem split ' ' map(_.toInt)

    // dermine nr of bins (or glasses in bin)
    val nrOfGlasses, nrOfBins = scala.math.sqrt(allGlassesInBins.size).toInt

    // verify correct nr of input values
    // Note: assertions in Scala don't use Java assertions and are always called.
    assert(nrOfBins * nrOfGlasses == allGlassesInBins.size, "The number of bins and glasses should be the same to be able to sort each glass in a seperate bin")

    // split problem by a space, convert to Int and split values into bins
    val bins = allGlassesInBins grouped nrOfBins

    // map bins to unsorted list of all GlassAndMoves per bin.
    val unsortedBins = bins map toGlassAndMoves toList

    // recursively calculate solutions
    val sortedBinsSolution = calcSolutions(List.empty[SortedBins], SortedBins.empty, unsortedBins)
    LOG.info("Nr of solutions found: %d" format sortedBinsSolution.size)

    // order solution by moves, than by name
    val sortedSolutions = sortedBinsSolution.sortBy(bin => (bin.totalMoves, bin.name))

    // take the best solution
    val solution = sortedSolutions.head
    LOG.info("Solution for problem '%s' is: %s" format (problem, solution))
    solution.toString
  }

  /**
   * Instance which represents a Glass in a bin and the number of moves
   * needed to clear the bin of the other glasses.
   *
   * Two instances of GlassAndMoves are equal when the Glass is the same.
   * Nr of moves not important.
   *
   * @param glass
   * @param movesToClear
   */
  private[scala] case class GlassAndMoves(glass: Glass.Value, movesToClear: Int) {
    override def equals(other: Any) = other match {
      case that: GlassAndMoves => (that canEqual this) && this.glass == that.glass
      case _ => false
    }
    override def canEqual(that: Any) = that.isInstanceOf[GlassAndMoves]

    override def hashCode() = glass.hashCode
  }

  /**
   * Represents a list of sorted bins, the bins only contain 1 type of Glass.
   * @param bins
   */
  private[scala] case class SortedBins(bins:List[GlassAndMoves]) {
    def size = bins.size
    def addBinWith(glass: GlassAndMoves) = new SortedBins(bins :+ glass)
    def containsBinWith(glass: GlassAndMoves) = bins.contains(glass)
    def notContainsBinWith(glass: GlassAndMoves) = !containsBinWith(glass)

    def totalMoves = bins map(_.movesToClear) sum
    def name = bins map(_.glass) mkString

    override def toString = "%s %d" format (name, totalMoves)
  }
  private[scala] object SortedBins {
    def apply(glass: GlassAndMoves) = new SortedBins(List(glass))
    def empty = new SortedBins(List())
  }

  /**
   * Types of Glass
   */
  private[scala] object Glass extends Enumeration {
    val Brown = Value("B")
    val Green = Value("G")
    val Clear = Value("C")
    val Red   = Value("R")
  }
}