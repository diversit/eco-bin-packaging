package eu.diversit.ing.java;

import com.google.common.base.Function;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;

import static com.google.common.collect.Lists.*;
import static java.util.Arrays.asList;

public class JavaEcoBinPackaging {

    private static final Logger LOG = LoggerFactory.getLogger(JavaEcoBinPackaging.class);

    public static String solve(final String problem) {
        // parse input into a List of bins. Each bin contains a List of glass counts.
        final List<List<Integer>> bins = parseInputIntoBins(problem);

        // transform bins into List of GlassAndMoves
        final List<List<GlassAndMoves>> unsortedBins = transform(bins, TO_GLASS_AND_MOVES_LIST);

        // Calculate all possible solutions
        final List<SortedBins> sortedBins = calcAllSolutions(unsortedBins);
        LOG.info("Nr of solutions found: {}", sortedBins.size());

        // Sort solutions by name and total moves
        final List<SortedBins> sortedSolutions = Ordering.from(BY_NAME_AND_TOTAL).sortedCopy(sortedBins);

        // Get best solution
        final String solution = sortedSolutions.get(0).toString();
        LOG.info("Solution for problem '{}' is: {}", problem, solution);
        return solution;
    }

    private static List<List<Integer>> parseInputIntoBins(final String problem) {
        final List<String> allValues = asList(problem.split(" "));
        int nrOfBins = validateInput(allValues);
        final List<Integer> allIntValues = transform(allValues, STRING_TO_INT);
        return partition(allIntValues, nrOfBins);
    }

    /**
     * Validates the number of bins matches the number of glass in the bins.
     * This should be equal to be able to put each glass in a seperate bin.
     */
    private static int validateInput(List<String> allValues) {
        // determine nr of bins (or glasses in bin)
        final int nrOrBins = (int)Math.sqrt(allValues.size());
        if((nrOrBins * nrOrBins) != allValues.size()) {
            throw new IllegalArgumentException("The number of bins and glasses should be the same to be able to sort each glass in a seperate bin");
        }
        return nrOrBins;
    }

    private static final Function<String,Integer> STRING_TO_INT = new Function<String, Integer>() {
        @Override
        public Integer apply(final String s) {
            return Integer.parseInt(s);
        }
    };

    private static final Function<List<Integer>, List<GlassAndMoves>> TO_GLASS_AND_MOVES_LIST = new Function<List<Integer>, List<GlassAndMoves>>() {
        @Override
        public List<GlassAndMoves> apply(final List<Integer> bin) {
            final long finalBinTotal = sumOf(bin);

            final List<GlassAndMoves> binWithGlass = newArrayList();
            for (int i = 0; i < bin.size(); i++) {
                final Glass glass = Glass.values()[i];
                binWithGlass.add(new GlassAndMoves(glass, movesToClearBinOfOtherGlass(finalBinTotal, bin.get(i))));
            }

            return binWithGlass;
        }

        private long movesToClearBinOfOtherGlass(final long finalBinTotal, final int currentGlassTotal) {
            return finalBinTotal - currentGlassTotal;
        }

        private long sumOf(final List<Integer> bin) {
            long binTotal = 0;
            for (Integer i : bin) {
                binTotal += i;
            }
            return binTotal;
        }
    };

    /**
     * Recursively calculate all posible solutions to sort the bins so that
     * each bin only contains 1 type of Glass.
     */
    private static List<SortedBins> calcAllSolutions(final List<List<GlassAndMoves>> unsortedBins) {
        return calcSolutions(SortedBins.empty(), unsortedBins);
    }

    private static List<SortedBins> calcSolutions(final SortedBins sortedBins, final List<List<GlassAndMoves>> unsortedBins) {
        if(sortedBins.size() == unsortedBins.size()) {
            return newArrayList(sortedBins);
        } else {
            final List<GlassAndMoves> currentBin = unsortedBins.get(sortedBins.size());
            List<SortedBins> solutionsList = newArrayList();
            for(GlassAndMoves glass : currentBin) {
                if(sortedBins.notContainsBinWith(glass)) {
                    List<SortedBins> someSolutions = calcSolutions(sortedBins.addBinWith(glass), unsortedBins);
                    solutionsList.addAll(someSolutions);
                }
            }
            return solutionsList;
        }
    }

    private static final Comparator<SortedBins> BY_NAME_AND_TOTAL = new Comparator<SortedBins>() {
        @Override
        public int compare(final SortedBins bin1, final SortedBins bin2) {
            return ComparisonChain.start()
                    .compare(bin1.totalMoves(), bin2.totalMoves())
                    .compare(bin1.name(), bin2.name())
                    .result();
        }
    };

    /**
     * ImmutableList of sorted bins. Each bin only contains 1 type of Glass.
     */
    static class SortedBins {
        private final ImmutableList<GlassAndMoves> bins;

        private SortedBins() {
            bins = ImmutableList.of();
        }

        private SortedBins(final List<GlassAndMoves> newBins) {
            bins = ImmutableList.copyOf(newBins);
        }

        public static SortedBins empty() {
            return new SortedBins();
        }

        public SortedBins addBinWith(final GlassAndMoves glass) {
            List<GlassAndMoves> newList = newArrayList(bins);
            newList.add(glass);
            return new SortedBins(newList);
        }

        public boolean containsBinWith(final GlassAndMoves glass) {
            return bins.contains(glass);
        }

        public boolean notContainsBinWith(final GlassAndMoves glass) {
            return !containsBinWith(glass);
        }

        public int size() {
            return bins.size();
        }

        public String name() {
            final StringBuilder nameBuilder = new StringBuilder();
            for(GlassAndMoves glass : bins) {
                nameBuilder.append(glass.getGlass());
            }
            return nameBuilder.toString();
        }

        public Long totalMoves() {
            long totalMoves = 0;
            for(GlassAndMoves glass : bins) {
                totalMoves += glass.getMovesToClear();
            }
            return totalMoves;
        }

        @Override
        public String toString() {
            return String.format("%s %d", name(), totalMoves());
        }
    }

    static class GlassAndMoves {
        private final Glass glass;
        private final long movesToClear;

        public GlassAndMoves(final Glass glass, final long movesToClear) {
            this.glass = glass;
            this.movesToClear = movesToClear;
        }

        public Glass getGlass() {
            return glass;
        }

        public long getMovesToClear() {
            return movesToClear;
        }

        @Override
        public int hashCode() {
            return glass.hashCode();
        }

        @Override
        public boolean equals(final Object o) {
            if(o instanceof GlassAndMoves) {
                final GlassAndMoves other = (GlassAndMoves) o;
                return glass.equals(other.glass);
            } else return false;
        }
    }

    enum Glass {
        Brown("B"),
        Green("G"),
        Clear("C"),
        Red("R");

        private String name;

        Glass(final String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
